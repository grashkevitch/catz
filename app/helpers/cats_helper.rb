module CatsHelper

	def like_button(cat)
        if cat.liked_by?(current_user)
           link_to "Unlike", unlike_cat_path(cat), method: :delete, class: 'button radius small alert'
        else
           link_to "Like", like_cat_path(cat), method: :post, class: 'button radius small'
        end  		
	end

end