module UsersHelper

	def follow_button(user)
        if user.is_followed_by?(current_user)
           link_to "Unfollow", unfollow_user_path(user), method: :delete, class: 'button radius small alert'
        else
           link_to "Follow", follow_user_path(user), method: :post, class: 'button radius small'
        end  		
	end
end
