class CatMailer < ActionMailer::Base
  default from: "no-reply@gregsexycats.herokuapp.com"

  def new_like(cat, liker)
    @liker = liker
    @user  = cat.user
    @cat   = cat
    @cat_image_url = Rails.env.production? ? @cat.image.url : GLOBAL[:site_url] + @cat.image.url
    mail(to: @user.email, subject: 'Someone just liked your cat!')
  end

  def new_unlike(cat, liker)
    @liker = liker
    @user  = cat.user
    @cat   = cat
    @cat_image_url = Rails.env.production? ? @cat.image.url : GLOBAL[:site_url] + @cat.image.url
    mail(to: @user.email, subject: 'Someone just un-liked your cat!')
  end

  
end
