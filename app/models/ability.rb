class Ability
  include CanCan::Ability

  def initialize(user)
    can :read, :all
    if user # current_user 
      can :manage, Cat.all do |cat|
        cat.user == user
      end
      can :crud, user
      can :create, Cat
      can :like, Cat
      can :unlike, Cat
      can :follow, User.all do |u|
        u != user
      end
      can :unfollow, user.users_followed do |u|
        true
      end
    end #if
  end #def
end #class 