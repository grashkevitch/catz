class Follow < ActiveRecord::Base
  attr_accessible :followed_user_id, :user_id
  belongs_to :follower, class_name: 'User', foreign_key: 'user_id'
  belongs_to :followed_user, class_name: 'User', foreign_key: 'followed_user_id'
  validate :no_self_follow
  validate :unique_follow

  def no_self_follow
    errors.add :base, 'You cannot follow yourself' if follower == followed_user
  end

  def unique_follow
    errors.add :base, 'This connection already exists' if Follow.where(followed_user_id: followed_user_id, user_id: user_id).any?
  end
end
