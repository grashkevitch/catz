class Cat < ActiveRecord::Base

  BREED_TYPES = ['Abyssinian', 'Aegean cat', 'Bengal', 'Burmese', 'Cymric', 'Havana Brown', 'Unknown']
  attr_accessible :bio, :name, :breed, :image
  mount_uploader :image, ImageUploader
  validates_presence_of :name, :user_id

  belongs_to :user

  has_many :likes

  has_many :likers, :through => :likes, :source => :user

  validates :breed, :inclusion => {:in => Cat::BREED_TYPES}

  default_scope { order('cats.created_at desc') }
  
  scope :by_breed, lambda { |breed| where('breed = ?', breed) }

  #Different approach to :by_bread
  BREED_TYPES.each do |breed|
    scope breed.gsub(' ','_').downcase.to_sym, lambda { where(breed: breed) }
  end 

  def self.not_owned_by(user)
    user ? where('user_id <> ?', user.id) : scoped
  end

  def liked_by?(user)
    likers.include? user
  end  

  def catname
    "#{name}" 
  end

  def to_s
    catname
  end

  def likes_count
    likes.size
  end

end
