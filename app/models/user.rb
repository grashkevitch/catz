class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :user_name, :first_name, :last_name, :city, :age,  :password, :password_confirmation, :remember_me, :image
  validates_presence_of :first_name,:last_name, :email, :user_name, :city, :age
  validates_uniqueness_of :email

  mount_uploader :image, ImageUploader

  before_validation :ensure_last_name_and_username

  #Cat likes stuff
  has_many :cats, :dependent => :destroy
  has_many :likes
  has_many :liked_cats, :through => :likes, :source => :cat

  #Follow stuff
  has_many :following_users, class_name: 'Follow', foreign_key: 'followed_user_id'
  has_many :followers, through: :following_users, source: :follower
  has_many :followings, class_name: 'Follow', foreign_key: 'user_id'
  has_many :users_followed, through: :followings, source: :followed_user

  SHOW_TOP_CATS_LIMIT = 3




  def ensure_last_name_and_username
    self.last_name = "Doe" if last_name.blank?
    self.user_name = "theuser" if user_name.blank?
  end



  def owns?(cat)
    cat.user == self
  end 

  def is_followed_by?(user)
    followers.include? user
  end

  def is_following?(user)
    users_followed.include? user
  end

  def unfollow!(user)
    followers.delete(user) if is_followed_by?(user)
  end

  def follow!(user)
    followers << user unless is_followed_by?(user)
  end

  def fullname
    "#{first_name} #{last_name}"  
  end

  def to_s
    fullname
  end

  def likes?(cat)
    liked_cats.include? cat
  end

  def like!(cat)
    liked_cats << cat unless likes?(cat)
  end

  def unlike!(cat)
    liked_cats.delete(cat) if likes?(cat)
  end

  def top_cats
    cats.limit(SHOW_TOP_CATS_LIMIT)
  end


end


