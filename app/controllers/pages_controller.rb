class PagesController < ApplicationController
  
  # GET /pages
  def index
    #@cats = Cat.not_owned_by(current_user)

    #Get all the cats of users I follow
    user_ids = current_user.users_followed.map(&:id) if current_user
    if current_user
    	@cats = Cat.where(user_id: user_ids).paginate(:page => params[:page], :per_page => 3)
    else
    	@cats = Cat.paginate(:page => params[:page], :per_page => 3)
    end
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
    @page = Page.find(params[:id])
  end
end
