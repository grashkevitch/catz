class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :load_user, :only => [:show, :liked_cats, :follow, :unfollow]
  load_and_authorize_resource

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @following_me = @user.followers
    @followed_by_me = @user.users_followed
  end

  def liked_cats
    @liked_cats = @user.liked_cats
  end

  def follow
    @user.follow!(current_user)
    redirect_to :back, notice: "You followed #{@user}"
  end

  def unfollow
    @user.unfollow!(current_user)
    redirect_to :back, notice: "You un-followed #{@user}"    
  end

  private
    def load_user
      if params[:id]
        @user = User.find(params[:id])        
      else    
        @user = current_user
      end
    end

end
