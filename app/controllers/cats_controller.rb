class CatsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :load_cat, :only => [:edit, :show, :update, :destroy]
  before_filter :load_user, :only => [:index] 
  #before_filter :authorize_cat!, :only => [:edit, :update, :destroy]
  load_and_authorize_resource

  # GET /user/cats
  def index
    @cats = @user.cats  
  end

  # GET /cats/1
  def show
    
  end

  # GET /cats/new
  def new
    @cat = Cat.new
  end

  # GET /cats/1/edit
  def edit
  end

  # POST /cats
  def create
    @cat = current_user.cats.new(params[:cat])
    
      if @cat.save
        flash[:notice] = "Created cat successfully"
        redirect_to @cat
      else
        render action: "new"
      end
  end

  # PUT /cats/1
  def update
      if @cat.update_attributes(params[:cat])
        redirect_to @cat, notice: 'Cat was successfully updated.'
      else
        render action: "edit"
      end
  end

  # DELETE /cats/1
  def destroy
    if @cat
      if @cat.destroy
        redirect_to cats_url, notice: 'Cat was deleted successfully'
      else
        flash[:alert] = 'Run. We have a zombie cat'
        redirect_to cats_url 
      end  
    else
        redirect_to cats_url, alert: 'Cat does not exist'
    end    
  end

 def like
   @cat = Cat.find(params[:id])
   current_user.like!(@cat)
   CatMailer.new_like(@cat, current_user).deliver
   redirect_to :back, notice: "You liked #{@cat}"
 end

def unlike
   @cat = Cat.find(params[:id])
   current_user.unlike!(@cat)
   CatMailer.new_unlike(@cat, current_user).deliver
   redirect_to :back, notice: "You un-liked #{@cat}"
 end

private
  def load_cat
      @cat = Cat.find(params[:id])
  end

  def load_user
    if params[:user_id]
      @user = User.find(params[:user_id])        
    else    
      @user = current_user
    end
  end

  def authorize_cat!
    deny unless current_user.owns?(@cat)
  end

end